﻿using System;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Task1.Models;

namespace Task1
{
    class Program
    {
        public static void Main(string[] args)
        {
            const string filesDir = "../../Files/";
            
            var clientXmlInputDir = Path.Combine(filesDir, "clientinfo_input.xml");
            var clientXmlOutputDir = Path.Combine(filesDir, "clientinfo_output.xml");
            var clientJsonOutputDir = Path.Combine(filesDir, "clientinfo_output.json");
            var workAddressXmlOutputDir = Path.Combine(filesDir, "workaddress_output.xml");
            var homeAddressJsonOutputDir = Path.Combine(filesDir, "homeaddress_output.json");

            try
            {
                BankClient client;

                using (var reader = new StreamReader(clientXmlInputDir))
                {
                    var xmlSerializer = new XmlSerializer(typeof(BankClientRaw));
                    client = BankClientConverter.Convert((BankClientRaw) xmlSerializer.Deserialize(reader));
                }

                using (var writer = new StreamWriter(clientXmlOutputDir))
                {
                    var xmlSerializer = new XmlSerializer(typeof(BankClient));
                    xmlSerializer.Serialize(writer, client);
                }

                using (var writer = new StreamWriter(clientJsonOutputDir))
                {
                    var jsonClient = JsonConvert.SerializeObject(client,
                        Formatting.Indented,
                        new IsoDateTimeConverter() {DateTimeFormat = "dd.MM.yyyy"});
                    writer.WriteLine(jsonClient);
                }

                using (var writer = new StreamWriter(workAddressXmlOutputDir))
                {
                    var xmlSerializer = new XmlSerializer(typeof(Address));
                    xmlSerializer.Serialize(writer, client.WorkAddress);
                }

                using (var writer = new StreamWriter(homeAddressJsonOutputDir))
                {
                    var jsonHomeAddress = JsonConvert.SerializeObject(client.HomeAddress,
                        Formatting.Indented);
                    writer.WriteLine(jsonHomeAddress);
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File {0} not found", e.FileName);
            }
        }
    }
}