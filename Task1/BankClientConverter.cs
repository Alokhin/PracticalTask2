﻿using System;
using Task1.Models;

namespace Task1
{
    public static class BankClientConverter
    {
        public static BankClient Convert(BankClientRaw client)
        {
            return new BankClient()
            {
                FirstName = client.FirstName,
                MiddleName = client.MiddleName,
                LastName = client.LastName,
                Phone = client.Phone,
                Email = client.Email,
                BirthDay = new DateTime(client.BirthDateYear,
                    client.BirthDateMonth, client.BirthDateDay),
                HomeAddress = new Address()
                {
                    City = client.HomeAddressCity,
                    State = client.HomeAddressState,
                    Line = client.HomeAddressLine1,
                    Zip = client.HomeAddressZip
                },
                WorkAddress = new Address()
                {
                    City = client.WorkAddressCity,
                    State = client.WorkAddressState,
                    Line = client.WorkAddressLine1,
                    Zip = client.WorkAddressZip
                }
            };
        }
    }
}