﻿using System.Xml.Serialization;

namespace Task1.Models
{
    [XmlRoot("cl")]
    public class BankClientRaw
    {
        [XmlElement("fn")]
        public string FirstName { get; set; }
        [XmlElement("ln")]
        public string LastName { get; set; }
        [XmlElement("mn")]
        public string MiddleName { get; set; }
        
        [XmlElement("p")]
        public string Phone { get; set; }
        [XmlElement("e")]
        public string Email { get; set; }
        
        [XmlElement("bd")]
        public int BirthDateDay { get; set; }
        [XmlElement("bm")]
        public int BirthDateMonth { get; set; }
        [XmlElement("by")]
        public int BirthDateYear { get; set; }
        
        [XmlElement("hl1")]
        public string HomeAddressLine1 { get; set; }
        [XmlElement("hc")]
        public string HomeAddressCity { get; set; }
        [XmlElement("hs")]
        public string HomeAddressState { get; set; }
        [XmlElement("hz")]
        public string HomeAddressZip { get; set; }
        
        [XmlElement("wl1")]
        public string WorkAddressLine1 { get; set; }
        [XmlElement("wc")]
        public string WorkAddressCity { get; set; }
        [XmlElement("ws")]
        public string WorkAddressState { get; set; }
        [XmlElement("wz")]
        public string WorkAddressZip { get; set; }
        
    }
}
