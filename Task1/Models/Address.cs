﻿namespace Task1.Models
{
    public class Address
    {
        public string State { get; set; }
        public string City { get; set; }
        public string Line { get; set; }        
        public string Zip { get; set; }
    }
}