﻿using System;
using System.Xml.Serialization;

namespace Task1.Models
{
    public class BankClient
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        
        public string Phone { get; set; }
        public string Email { get; set; }
        
        [XmlElement(DataType = "date")]
        public DateTime BirthDay { get; set; }

        public Address HomeAddress { get; set; }
        public Address WorkAddress { get; set; }
        
    }
}