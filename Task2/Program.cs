﻿using System;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Task2
{
    class Program
    {
        public static void Main(string[] args)
        {
            const string filesDir = "../../Operations/";
            var files = Directory.GetFiles(filesDir);
            
            Operation maxOperation = null;
            
            foreach (var file in files)
            {
                var extension = Path.GetExtension(file);
                try
                {
                    using (var reader = new StreamReader(file))
                    {
                        Operation operation = null;
                        
                        switch (extension)
                        {
                            case ".xml":
                                var operationXmlSerializer = new XmlSerializer(typeof(Operation));
                                operation = (Operation) operationXmlSerializer.Deserialize(reader);
                                break;
                            case ".json":
                                operation = JsonConvert.DeserializeObject<Operation>(reader.ReadToEnd());
                                break;
                            default:
                                throw new Exception($"Unsupported extension {extension}.");
                        }

                        if (maxOperation == null || operation.Amount > maxOperation.Amount)
                        {
                            maxOperation = operation;
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error has occurred in file '{0}':", file);
                    Console.WriteLine("Message: {0}", e.Message);
                }
            }

            if (maxOperation != null)
            {
                Console.WriteLine("Operation with max amount:\n type: {0} \n amount: {1} \n date: {2}",
                    maxOperation.OperationType, maxOperation.Amount, maxOperation.Date);
            }
        }
    }
}