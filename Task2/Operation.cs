﻿using System;
using System.Xml.Serialization;

namespace Task2
{
    public enum OperationType
    {
        [XmlEnum("expense")]
        Credit,
        [XmlEnum("income")]
        Debit
    }
    
    [XmlRoot("operation")]
    public class Operation
    {
        public decimal Amount { get; set; }    
        public DateTime Date { get; set; }    
        [XmlAttribute("type")]
        public OperationType OperationType { get; set; }            
    }
}